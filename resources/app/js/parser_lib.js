var num_line 		= 1;
var active_tab		= 1;
var exec_test		= '/var/www/parser-cli.ru/test.php';


//Сброс скролинга body
document.onwheel = function(e)
{
	//Отменяем прокрутку страницы при прокрутке на левом меню
	if(e.target.className == 'button_layer' || e.target.id == 'tab_list') return false;
}

//Обработчик
window.onload = function(){

		document.getElementById('tab_' + window.active_tab).style.border = '2px solid #DC143C';
		document.getElementById("tab_list").onwheel = function(e){

			arTOP = document.getElementsByClassName('button_layer');

			if(e.deltaY < 0)						//Если крутим вверх - листаем влево
			{
				this.insertBefore(arTOP[arTOP.length - 1], this.firstChild);	//Последний элемент вставляем перед первым
			}

			if(e.deltaY > 0)						//Если крутим вниз - листаем вправо
			{
				this.appendChild(arTOP[0]);											//Последний элемент вставляем последним
			}

		}
}


function AddComLine()
{
	window.num_line = window.num_line + 1;
	var tr 			= document.createElement('tr');
	tr.innerHTML 	= '	<td class="numer">' + window.num_line + '</td>\
						<td class="group"><input type="checkbox" name="chgroup"></td>\
						<td class="command"><input type="" name="arr_exec" class="input_run"></td>\
						<td class="numer">' + window.num_line + '</td>\
						<td class="but_run"><button class="sbutton" onclick="SendRun(this);">SEND</button></td>\
						<td class="del"><button class="sbutton" onclick="DelComLine(this);">DEL</button></td>\
						<td class="status">NULL</td>';

	area_change.insertBefore(tr, area_change.children[area_change.children.length-1]);
	CreateView(window.num_line);
}

function DelComLine(obj)
{
	var parent 	= obj.parentElement.parentElement;  		//<tr>
	var id = parent.getElementsByClassName('numer')[0].innerHTML;

	area_change.removeChild(parent);									//delete URL
	contents.removeChild(document.getElementById('con_' + id));			//delete CONSOLE()
	if(window.active_tab != id)
	{
		tab_list.removeChild(document.getElementById('tab_' + id));			//delete TAB
	}
	else
	{
		window.active_tab = 1;
		document.getElementById('con_1').style.display = 'block';
		document.getElementById('tab_1').style.top = '8px';

		tab_list.removeChild(document.getElementById('tab_' + id));			//delete TAB
	}
}

function CreateView(id)
{
	//Check hidden area for console (con_*)
	if(document.getElementById('con_' + id) == null)
	{
		var newid = document.getElementById(id);				//get id
		var contents = document.getElementById('contents');		//get object area contents
		var div = document.createElement('div');
		div.id = 'con_' + id;									//append id
		div.className = 'content_view';							//append class
		div.style.display = 'none';								//set hidden response
		//div.style.overflow = 'auto';
		contents.appendChild(div);								//add as child

		var tab = document.createElement('button');
		tab.innerHTML = id;
		tab.id = 'tab_' + id;
		tab.className = 'button_layer';
		tab.onclick = function(){

			ClickTab(tab);
		}
		document.getElementById('tab_list').appendChild(tab);
	}
}

function SendRun(obj)
{
	var parent 	= obj.parentElement.parentElement;  		//<tr>
	var data 	= parent.children[2].children[0].value;		//<tr>< class="input_run">.value
	var id = parent.getElementsByClassName('numer')[0].innerHTML;
	parent.children[6].innerHTML = 'SEND';

	const { exec } = require('child_process');
	exec('php ' + exec_test +' -url '+data, (error, stdout, stderr) => {
		  if (error)
		  {
		    console.error(`exec error: ${error}`);
		    return;
		  }
		  var out = `${stdout}`;
		  //out = out.replace(/ /g, '&nbsp;');
		  out = out.replace(/\r\n|\r|\n/g,"<br/>");
		  parent.children[6].innerHTML = 'GET';

		  if('${stderr}' != '')
		  {
		  	//docume
		  }
		  document.getElementById('con_' + id).innerHTML = out;
	});
}

function ClickTab(tab_obj)
{
	//Убираем маркер активности
	document.getElementById('tab_' + window.active_tab).style.border = '2px solid gray';

	document.getElementById('con_' + window.active_tab).style.display = 'none';
	document.getElementById('con_' + tab_obj.id.replace('tab_', '')).style.display = 'block';
	window.active_tab = tab_obj.id.replace('tab_', '');
	//Делаем активным
	document.getElementById('tab_' + window.active_tab).style.border = '2px solid #DC143C';
}

function SendGroupRun()
{
	const { exec } = require('child_process');
	exec('php ' + exec_test, (error, stdout, stderr) => {
	  if (error) {
	    console.error(`exec error: ${error}`);
	    return;
	  }
	  console.log(`stdout: ${stdout}`);
	  console.log(`stderr: ${stderr}`);
	});
}
